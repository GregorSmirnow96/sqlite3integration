/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author im5no
 */
public class SQLTable
{
    private List<String> columnNames;
    private final List<List<String>> tuples;
    
    public SQLTable(ResultSet resultSet)
    {
        columnNames = new ArrayList<>();
        tuples = new ArrayList<>();
            
        try
        {
            ResultSetMetaData setMetaData = resultSet.getMetaData();
            int columnCount = setMetaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++)
            {
                String nextColumnName = setMetaData.getColumnName(i);
                columnNames.add(nextColumnName);
            }
            
            while (resultSet.next())
            {
                List<String> nextTuple = new ArrayList<>();
                for (int i = 0; i < columnCount; i++)
                {
                    String columnName = columnNames.get(i);
                    String nextValue = resultSet.getString(columnName);
                    nextTuple.add(nextValue);
                }
                tuples.add(nextTuple);
            }
        }
        catch (SQLException e)
        {
            SystemAlerter.alert(e);
        }
    }
    
    public List<String> getColumnNames()
    {
        return columnNames;
    }
    
    public List<List<String>> getTuples()
    {
        return tuples;
    }
}