/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration;

/**
 *
 * @author im5no
 */
public class SQLCommand
{
    private final String text;
    
    public SQLCommand(String text)
    {
        this.text = text;
    }
    
    public String getText()
    {
        return text;
    }
}