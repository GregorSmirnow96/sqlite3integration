/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration;

import sqlite3integration.DataAccessLayer.DAL;

/**
 *
 * @author im5no
 */
public class SQLite3Integration
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("Highschoolers");
        DAL.getHighschoolers().forEach(
            person ->
                System.out.println("{ " + person.ID + ", " + person.name + ", " + person.grade + " }"));
        
        System.out.println("Friends");
        DAL.getFriend().forEach(
            friend ->
                System.out.println("{ " + friend.ID1 + ", " + friend.ID2 + " }"));
        
        System.out.println("Likes");
        DAL.getLikes().forEach(
            likes ->
                System.out.println("{ " + likes.ID1 + ", " + likes.ID2 + " }"));
    }
}