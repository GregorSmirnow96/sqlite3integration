/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.DataAccessLayer;

import sqlite3integration.SQLCommand;
import sqlite3integration.SQLiteConnection;
/**
 *
 * @author im5no
 */
public abstract class DataRetreiver
{
    protected final SQLiteConnection SQLiteConnection;
    protected final SQLCommand command;
    
    public DataRetreiver(String queryString)
    {
        SQLiteConnection = new SQLiteConnection(DAL.getConnectionString());
        command = new SQLCommand(queryString);
    }
}
