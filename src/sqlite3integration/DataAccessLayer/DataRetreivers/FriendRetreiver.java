/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.DataAccessLayer.DataRetreivers;

import java.util.ArrayList;
import java.util.List;
import sqlite3integration.DataAccessLayer.DataRetreiver;
import sqlite3integration.Entities.Friend;
import sqlite3integration.SQLTable;

/**
 *
 * @author im5no
 */
public class FriendRetreiver extends DataRetreiver
{
    private static final String QUERY_STRING = "SELECT * FROM Friend;";
    private static final int ID1_INDEX = 0;
    private static final int ID2_INDEX = 1;
    
    public FriendRetreiver()
    {
        super(QUERY_STRING);
    }
    
    public List<Friend> getData()
    {
        SQLTable friendTable =
            SQLiteConnection.executeCommandWithReturn(command);
        
        List<Friend> friends = new ArrayList<>();
        friendTable.getTuples().forEach(
            friend ->
            {
                int id1 = Integer.parseInt(friend.get(ID1_INDEX));
                int id2 = Integer.parseInt(friend.get(ID2_INDEX));
                Friend nextFriend = new Friend(
                    id1,
                    id2);
                
                friends.add(nextFriend);
            });
        
        return friends;
    }
}