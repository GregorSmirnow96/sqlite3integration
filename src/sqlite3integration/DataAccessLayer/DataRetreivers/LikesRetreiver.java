/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.DataAccessLayer.DataRetreivers;

import java.util.ArrayList;
import java.util.List;
import sqlite3integration.DataAccessLayer.DataRetreiver;
import sqlite3integration.Entities.Likes;
import sqlite3integration.SQLTable;

/**
 *
 * @author im5no
 */
public class LikesRetreiver extends DataRetreiver
{
    private static final String QUERY_STRING = "SELECT * FROM Likes;";
    private static final int ID1_INDEX = 0;
    private static final int ID2_INDEX = 1;
    
    public LikesRetreiver()
    {
        super(QUERY_STRING);
    }
    
    public List<Likes> getData()
    {
        SQLTable likesTable =
            SQLiteConnection.executeCommandWithReturn(command);
        
        List<Likes> likes = new ArrayList<>();
        likesTable.getTuples().forEach(
            like ->
            {
                int id1 = Integer.parseInt(like.get(ID1_INDEX));
                int id2 = Integer.parseInt(like.get(ID2_INDEX));
                Likes nextLikes = new Likes(
                    id1,
                    id2);
                
                likes.add(nextLikes);
            });
        
        return likes;
    }
}