/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.DataAccessLayer.DataRetreivers;

import java.util.ArrayList;
import java.util.List;
import sqlite3integration.DataAccessLayer.DataRetreiver;
import sqlite3integration.Entities.Highschooler;
import sqlite3integration.SQLTable;

/**
 *
 * @author im5no
 */
public class HighschoolerRetreiver extends DataRetreiver
{
    private static final String QUERY_STRING = "SELECT * FROM Highschooler;";
    private static final int ID_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int GRADE_INDEX = 2;
    
    public HighschoolerRetreiver()
    {
        super(QUERY_STRING);
    }
    
    public List<Highschooler> getData()
    {
        SQLTable highschoolerTable =
            SQLiteConnection.executeCommandWithReturn(command);
        
        List<Highschooler> highschoolers = new ArrayList<>();
        highschoolerTable.getTuples().forEach(
            highschooler ->
            {
                int id = Integer.parseInt(highschooler.get(ID_INDEX));
                String name = highschooler.get(NAME_INDEX);
                int grade = Integer.parseInt(highschooler.get(GRADE_INDEX));
                Highschooler nextHighschooler = new Highschooler(
                    id,
                    name,
                    grade);
                
                highschoolers.add(nextHighschooler);
            });
        
        return highschoolers;
    }
}