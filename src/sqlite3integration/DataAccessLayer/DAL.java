/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.DataAccessLayer;

import java.io.File;
import sqlite3integration.DataAccessLayer.DataRetreivers.HighschoolerRetreiver;
import java.util.List;
import sqlite3integration.DataAccessLayer.DataRetreivers.FriendRetreiver;
import sqlite3integration.DataAccessLayer.DataRetreivers.LikesRetreiver;
import sqlite3integration.Entities.Friend;
import sqlite3integration.Entities.Highschooler;
import sqlite3integration.Entities.Likes;

/**
 *
 * @author im5no
 */
public class DAL
{
    public static String getConnectionString()
    {
        String pathToDatabaseFromSource =
            "\\src\\sqlite3integration\\DatabaseFiles\\School.db";
        String currentLocation = (new File("").getAbsolutePath());
        String fullPath =
            currentLocation + pathToDatabaseFromSource;
        
        return "jdbc:sqlite:" + fullPath;
    }
    
    public static List<Highschooler> getHighschoolers()
    {
        HighschoolerRetreiver dataRetriever = new HighschoolerRetreiver();
        return dataRetriever.getData();
    }
    
    public static List<Friend> getFriend()
    {
        FriendRetreiver dataRetriever = new FriendRetreiver();
        return dataRetriever.getData();
    }
    
    public static List<Likes> getLikes()
    {
        LikesRetreiver dataRetriever = new LikesRetreiver();
        return dataRetriever.getData();
    }
}