/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 *
 * @author im5no
 */
public class SQLiteConnection
{
    Optional<Connection> connection;
    String connectionString;
    
    public SQLiteConnection(String connectionString)
    {
        connection = Optional.empty();
        this.connectionString = connectionString;
    }
    
    public SQLTable executeCommandWithReturn(SQLCommand command)
    {
        open();
        
        SQLTable queriedTable = null;
        
        if (!connection.isPresent())
            return null;
        
        try
        {
            ResultSet resultSet =
                connection
                    .get()
                    .createStatement()
                    .executeQuery(command.getText());
            queriedTable = new SQLTable(resultSet);
        }
        catch (SQLException e)
        {
            SystemAlerter.alert(e);
        }
        
        close();
        
        return queriedTable;
    }
    
    public void executeCommand(SQLCommand command)
    {
        open();
        
        if (connection.isPresent())
            return;
        
        try
        {
            connection
                .get()
                .createStatement()
                .executeQuery(command.getText());
        }
        catch (SQLException e)
        {
            SystemAlerter.alert(e);
        }
        
        close();
    }
        
    private void open()
    {
        try
        {
            Connection nullableConnection =
                DriverManager.getConnection(connectionString);
            connection = Optional.ofNullable(nullableConnection);
        }
        catch (SQLException e)
        {
            SystemAlerter.alert(e);
        }
    }
    
    private void close()
    {
        connection.ifPresent(
            theConnection -> 
            {
                try
                {
                    theConnection.close();
                }
                catch (SQLException e)
                {
                    SystemAlerter.alert(e);
                }
            });
    }
}