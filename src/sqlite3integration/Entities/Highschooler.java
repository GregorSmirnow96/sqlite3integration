/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlite3integration.Entities;

/**
 *
 * @author im5no
 */
public class Highschooler
{
    public int ID;
    public String name;
    public int grade;
    
    public Highschooler(
        int ID,
        String name,
        int grade)
    {
        this.ID = ID;
        this.name = name;
        this.grade = grade;
    }
}